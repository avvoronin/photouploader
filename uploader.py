#!/usr/bin/env python

import argparse
import os
import sqlite3 as sq
import datetime
import time
#from tkinter import *
import hashlib
import exifread
import shutil


''' 
02.07.2019 starting new release with storing existed files info in a SQLite database in  destination folder.
'''

class Dbhelper():
    LAUNCHES_TAB_NAME = "LaunchesHistory"
#    FILES_TAB_NAME = "ProcessedFiles"
    DEST_FILES_TAB_NAME = "AllFilesInfo"
    FILES_FOR_MOVING_TAB_NAME = "FilesChangingQueue"
    DB_FILENAME = "uploaderhistory.db"
    


    #AVAILABLE_EXTENSIONS_FOR_DATE_EXTRACTING = (".JPG",".JPEG")

    def __init__(self,destfolder):
        #self.conn = sq.connect(Dbhelper.DB_FILENAME)
        #TO-DO use createconnection method?
        self.rootfolder = destfolder
        self.filename = os.path.join(destfolder,Dbhelper.DB_FILENAME)
        self.connection =  sq.connect(self.filename)

        '''Creating launches history table'''

        query = '''CREATE TABLE if not exists {} (
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    Executedate DateTime NOT NULL,
                    Params Text NOT NULL)'''.format(Dbhelper.LAUNCHES_TAB_NAME)

        self.connection.execute(query)

##        '''Creating files table'''
##        query = '''CREATE TABLE if not exists {} (
##                    launch_id INTEGER NOT NULL,
##                    filepath VARCHAR NOT NULL,
##                    extension VARCHAR NULL,
##                    filehash VARCHAR,
##                    exifdate DATETIME,
##                    subfolder VARCHAR,
##                    duplicate int
##                    )'''.format(Dbhelper.FILES_TAB_NAME)
##
##        self.connection.execute(query)
        
        '''
            Main table, that stores information about all files in folder
        '''
        query = '''CREATE TABLE if not exists {} (
                    file_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    filename VARCHAR NOT NULL,
                    extension VARCHAR NULL,
                    size INTEGER NOT NULL,
                    filehash VARCHAR NOT NULL,
                    creationdate DATETIME NOT NULL,
                    creationdatesource VARCHAR NOT NULL CHECK (creationdatesource in ('Exif','File_creation_date','Manual')),
                    subfolder VARCHAR NULL, 
                    lastmodifiedbylaunch INTEGER NULL
                    )'''.format(Dbhelper.DEST_FILES_TAB_NAME)
        #           
        #creationdatesource VARCHAR NOT NULL CHECK (creationdatesource in 'Exif','File_creation_date','Manual'),
        
        self.connection.execute(query)

        '''
            Creating table with filenames that should be renamed/moved
        '''
        query = '''CREATE TABLE if not exists {} (
                    row_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    launch_id INTEGER NOT NULL,
                    file_id INTEGER NULL,
                    source_file_id INTEGER NULL,
                    action VARCHAR NOT NULL CHECK (action in ('Move','Upload')),
                    sourcefilepath VARCHAR NOT NULL,
                    subfolder VARCHAR NOT NULL,
                    destfilename VARCHAR NOT NULL
                    )'''.format(Dbhelper.FILES_FOR_MOVING_TAB_NAME)

        self.connection.execute(query)

        self.connection.commit()
        #self.connection.close()

    
##    def start_batch(self):
##        '''
##        Starts batch for sending multiple queries in a single batch
##        '''
##        self.connection = sq.connect(Dbhelper.DB_FILENAME)
##
##    def end_batch(self):
##        self.connection.commit()
##        self.connection.close()

##    def createconnection(self):
##        return sq.connect(self.filename) 

    def commit(self):
        self.connection.commit()



    def create_launch(self,params):
        query = "INSERT INTO {} (Executedate, Params) Values (?, ?)".format(Dbhelper.LAUNCHES_TAB_NAME)
        self.connection.execute(query,(datetime.datetime.now(),str(params)))
        launchidquery = "SELECT last_insert_rowid()"
        cursor = self.connection.cursor()
        cursor.execute(launchidquery)

        launchid = cursor.fetchone()[0]
        '''Сreating table for storing single launch data'''
        tabname = self.__launchtabname__(launchid)

        query = '''CREATE TABLE if not exists {} (
                    source_file_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    sourcefilepath VARCHAR NOT NULL,
                    extension VARCHAR NULL,
                    size INTEGER NOT NULL,
                    filehash VARCHAR NOT NULL,
                    creationdate DATETIME NOT NULL,
                    creationdatesource VARCHAR NOT NULL CHECK (creationdatesource in ('Exif','File_creation_date')),
                    isduplicate INT DEFAULT(0),
                    subfolder VARCHAR NOT NULL
                    )'''.format(tabname)
        self.connection.execute(query)
        
        
        
        self.connection.commit()
        #self.connection.close()
        return launchid

    def __launchtabname__(self, launchid):
        return "DataFromLaunch_{}".format(launchid)

    
    
    '''
    def insert_fileinfo(self, launchid,filepath, withchecksum, source ):
        #TO-Do Raise if batch update not started
        #if self.connection is None:
        #    raise ""
        shortname, fileExtension = os.path.splitext(filepath) 
        if withchecksum:
            filehash = md5(fullfilename)
        else:
            filehash = None
        #print("fullfilename={} fileExtension = {} hash={}".format(fullfilename,fileExtension,filehash))

        if fileExtension in Dbhelper.AVAILABLE_EXTENSIONS_FOR_DATE_EXTRACTING:
            createdate = exiftag(filepath,"EXIF DateTimeOriginal")
             #q = datetime.datetime.strptime(createdate, "%Y:%m:%d %H:%M:%S")
             
            createdate = (datetime.datetime.strptime(createdate, "%Y:%m:%d %H:%M:%S") if createdate is not None else None)
        else:
            createdate = None
            
        
        
        query = "INSERT INTO {} (launch_id, filepath,extension,filehash,exifdate) Values(?,?,?,?,?)".format(Dbhelper.FILES_TAB_NAME if source else Dbhelper.DEST_FILES_TAB_NAME)
        self.connection.execute(query,(launchid,filepath,fileExtension,filehash,createdate))
            

    def generatesubfolderpath(self, launchid):

        connection = sq.connect(Dbhelper.DB_FILENAME)

        query = "UPDATE {} SET subfolder = strftime('%Y{}%m' ,exifdate) WHERE exifdate IS NOT NULL AND launch_id=?".format(Dbhelper.FILES_TAB_NAME,os.sep)
        connection.execute(query,(launchid,))

        query = "UPDATE {} SET subfolder = strftime('%Y{}%m' ,exifdate) WHERE exifdate IS NOT NULL AND launch_id=?".format(Dbhelper.DEST_FILES_TAB_NAME,os.sep)
        connection.execute(query,(launchid,))

        query = "UPDATE {} SET subfolder = '{}' WHERE exifdate IS NULL AND launch_id=?".format(Dbhelper.FILES_TAB_NAME,datetime.now.strftime("%Y%m%d_%H%M%S"))
        connection.execute(query,(launchid,))

        
        connection.commit()
        connection.close()

    def markduplicatefiles(self, launchid, CheckDuplicates):
        connection = sq.connect(Dbhelper.DB_FILENAME)
        if CheckDuplicates:
            query = "update {0} SET duplicate = 1 WHERE launch_id = ? and exists(SELECT * FROM {1} WHERE launch_id = ? AND {0}.filehash = {1}.filehash);".format(Dbhelper.FILES_TAB_NAME,Dbhelper.DEST_FILES_TAB_NAME)
            connection.execute(query,(launchid,launchid))
            query = "update {} SET duplicate = 0 WHERE launch_id = ? and duplicate is null;".format(Dbhelper.FILES_TAB_NAME)
            connection.execute(query,(launchid,))
        else:
            query = "UPDATE {} SET duplicate = 0 WHERE launch_id = ?".format(Dbhelper.FILES_TAB_NAME)
            connection.execute(query,(launchid,))

        connection.commit()
    '''
    def insertnewfileinfo(self, launchid, filename, extension, size, filehash, creationdate, creationdatesource,subfolder):

            query = "INSERT INTO {} (filename,extension,size,filehash,creationdate,creationdatesource,subfolder,lastmodifiedbylaunch) Values(?,?,?,?,?,?,?,?)".format(Dbhelper.DEST_FILES_TAB_NAME)
            connection = self.connection
            cursor=connection.cursor()
            cursor.execute(query,(filename, extension, size, filehash, creationdate, creationdatesource,subfolder,launchid))
            lastid = cursor.lastrowid
            connection.commit()
            #connection.close()
            return lastid

    def insertsourcefileinfo(self, launchid, filepath, extension, size,  filehash, creationdate, creationdatesource):

            subfolder = os.path.join(creationdate.strftime("%Y"),creationdate.strftime("%m"))
            query = "INSERT INTO {} (sourcefilepath,extension, size, filehash,creationdate,creationdatesource,subfolder) Values(?,?,?,?,?,?,?)".format(self.__launchtabname__(launchid))
            connection = self.connection
            cursor=connection.cursor()
            cursor.execute(query,(filepath, extension, size, filehash, creationdate, creationdatesource, subfolder))
            lastid = cursor.lastrowid
            #connection.commit()
            #connection.close()
            return lastid

    def changefiledestination(self, fileid, subfolder, filename):
           query = "UPDATE {} SET subfolder = ?, filename = ? WHERE file_id =?".format(Dbhelper.DEST_FILES_TAB_NAME)
           self.connection.execute(query,(subfolder,filename, fileid))

    def getsourcefileinfo(self, launchid, source_file_id):
        '''
        gets file information from table that stores info about files we want to upload
        '''
        sourcetabname = self.__launchtabname__(launchid)
        query = 'SELECT  sourcefilepath, extension, size,filehash,creationdate, creationdatesource FROM {} WHERE source_file_id = ? AND isduplicate = 0'.format(sourcetabname)
        cursor = self.connection.cursor()
        cursor.execute(query,(source_file_id,))
        return cursor.fetchone()

    def getfileinfo(self, file_id):
        '''
        gets file information from table  with info about files in datastore 
        '''
        query = 'SELECT  filename, subfolder, extension, size,filehash,creationdate, creationdatesource FROM {} WHERE file_id = ?'.format(Dbhelper.DEST_FILES_TAB_NAME)
        cursor = self.connection.cursor()
        cursor.execute(query,(file_id,))
        return cursor.fetchone()



    def markduplicatesinsource(self,launchid):
        sourcetabname = self.__launchtabname__(launchid)
        query = '''UPDATE {0} SET isduplicate = 1
                   WHERE EXISTS(SELECT * FROM {1} WHERE {0}.size = {1}.size AND {0}.filehash = {1}.filehash);
                '''.format(sourcetabname, Dbhelper.DEST_FILES_TAB_NAME)
        self.connection.execute(query)
        self.connection.commit()


    def fillqueue(self,launchid):
        '''
        Fill files list that should moved or uploaded
        '''
        sourcetabname = self.__launchtabname__(launchid)
        query = "SELECT DISTINCT SubFolder FROM {}".format(sourcetabname)
        connection = self.connection
        
        cursor = connection.cursor()
        cursor.execute(query)
        results = cursor.fetchall()
        '''
            using temptable to set order number of file
        '''
        for subFoldername in results:
            subfoldernamestr = subFoldername[0]
            temptabname = "tmp{}".format(subfoldernamestr.replace(os.sep,""))
            createtablequery = '''CREATE TEMPORARY TABLE {} (
                    NPP INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    source_file_id INTEGER NULL,
                    file_id INTEGER NULL,
                    extension VARCHAR NOT NULL,
                    action VARCHAR NOT NULL,
                    subfolder VARCHAR NOT NULL,
                    filename VARCHAR NULL
                    )'''.format(temptabname)
            connection.execute(createtablequery)

            fileNPPquery = '''INSERT INTO {}(source_file_id, file_id, extension, action,subfolder)
                  SELECT source_file_id, file_id, extension, action,subfolder FROM (
                  SELECT source_file_id, NULL As file_id, creationdate, extension, 'Upload' as action, subfolder FROM {} WHERE isduplicate = 0 AND subfolder = ?
                  UNION ALL
                  SELECT NULL, file_id, creationdate, extension, 'Move', subfolder FROM {} WHERE subfolder = ?
                  ) s
                  ORDER BY s.creationdate
                  '''.format(temptabname,sourcetabname,Dbhelper.DEST_FILES_TAB_NAME)
            #print(fileNPPquery)
            connection.execute(fileNPPquery,(subfoldernamestr,subfoldernamestr))
            updatequery = "UPDATE {} SET filename = '{}'||case when length(NPP)>5 then NPP else substr('00000'||NPP,-5,5) end||extension".format(temptabname, "F")
            connection.execute(updatequery)
            connection.commit()
            '''
            adding files to queue
            '''
            '''
                1. Files to be moved
            '''
            insertquery = '''INSERT INTO {} (launch_id, file_id,  action, sourcefilepath, subfolder, destfilename)
                             SELECT ?,ExistedFiles.file_id, tmp.action, ?||ExistedFiles.filename, tmp.subfolder,tmp.filename
                             FROM {} AS tmp INNER JOIN {} as ExistedFiles ON tmp.file_id = ExistedFiles.file_id
                             WHERE tmp.action = 'Move' AND (tmp.subfolder||tmp.filename)<>(ExistedFiles.subfolder||ExistedFiles.filename)
                             ORDER BY tmp.NPP DESC
                             '''.format(Dbhelper.FILES_FOR_MOVING_TAB_NAME,temptabname,Dbhelper.DEST_FILES_TAB_NAME)
            connection.execute(insertquery,(launchid,os.path.join(self.rootfolder,subfoldernamestr) + os.sep))
            '''
                2. Files to be uploaded
            '''
            insertquery = '''INSERT INTO {} (launch_id, source_file_id,  action, sourcefilepath, subfolder, destfilename)
                             SELECT ?,NewFiles.source_file_id, tmp.action, NewFiles.sourcefilepath, tmp.subfolder,tmp.filename
                             FROM {} AS tmp INNER JOIN {} as NewFiles ON tmp.source_file_id = NewFiles.source_file_id
                             WHERE tmp.action = 'Upload'
                             ORDER BY tmp.NPP DESC
                             '''.format(Dbhelper.FILES_FOR_MOVING_TAB_NAME,temptabname,sourcetabname)
            connection.execute(insertquery,(launchid,))
            connection.commit()


    def getqueue(self,launchid):        
        '''
        Gets all data for processing
        '''
        cursor = self.connection.cursor()
        query = "SELECT row_id,file_id,source_file_id, action, subfolder, destfilename FROM {} WHERE launch_id=? ORDER BY row_id".format(Dbhelper.FILES_FOR_MOVING_TAB_NAME)
        cursor.execute(query,(launchid,))
        return  cursor.fetchall()    



                            
    
##    def fillResultTable(self, launchid,DestFolder,DuplicatesFolder):
##        connection = sq.connect(Dbhelper.DB_FILENAME)
##        #Step 1. Fill Destination folder for duplicate files
##        duplicatestemptabname = "tmpduplicates"
##        duplicatecreatetablequery = '''CREATE TEMPORARY TABLE {} (
##                    NPP INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
##                    filepath VARCHAR NOT NULL
##                    )'''.format(duplicatestemptabname)
##        connection.execute(duplicatecreatetablequery)
##        
##        fileNPPquery = '''INSERT INTO {}(filepath)
##                          SELECT filepath FROM {} WHERE launch_id = ? AND duplicate = 1
##                       '''.format(duplicatestemptabname,Dbhelper.FILES_TAB_NAME)
##        connection.execute(fileNPPquery,(launchid,))
##
##        #destinationfoldertemplate = "{}//Duplicate_".format(DuplicatesFolder)
##        destinationfoldertemplate = os.path.join(DuplicatesFolder,"Duplicate_")
##        resulttablequery = '''INSERT INTO {} (launch_id,SourceFilepath,DestFilepath)
##                              SELECT {}, filepath, '{}' || substr('00000'||NPP,-5,5) FROM {}
##                           '''.format(Dbhelper.FILES_FOR_MOVING_TAB_NAME, launchid, destinationfoldertemplate, duplicatestemptabname)
##        connection.execute(resulttablequery)
##        
##
##        #Step 2. Fill destination folder for files that should moved from source and for files that sould be renamed in destnation folder
##        query = "SELECT DISTINCT SubFolder FROM {} WHERE launch_id = ? ORDER BY Replace(SubFolder,'{}','')".format(Dbhelper.FILES_TAB_NAME, os.sep)
##        cursor = connection.cursor()
##        cursor.execute(query, (launchid,))
##        results = cursor.fetchall()
##        for subFoldername in results:
##            subfoldernamestr = subFoldername[0]
##            temptabname = "tmp{}".format(subfoldernamestr.replace(os.sep,""))
##            createtablequery = '''CREATE TEMPORARY TABLE {} (
##                    NPP INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
##                    filepath VARCHAR NOT NULL
##                    )'''.format(temptabname)
##            #print(createtablequery)
##            connection.execute(createtablequery)
##
##            '''
##                using temptable to set order number of file
##            '''
##            fileNPPquery = '''INSERT INTO {}(filepath)
##                  SELECT filepath FROM (
##                  SELECT filepath, exifdate FROM {} WHERE launch_id = ? AND duplicate = 0 AND subfolder = ?
##                  UNION ALL
##                  SELECT filepath, exifdate FROM {} WHERE launch_id = ? AND subfolder = ?
##                  ) s
##                  ORDER BY s.exifDate
##                  '''.format(temptabname,Dbhelper.FILES_TAB_NAME,Dbhelper.DEST_FILES_TAB_NAME)
##            #print(fileNPPquery)
##            connection.execute(fileNPPquery,(launchid,subfoldernamestr,launchid,subfoldernamestr))
##
##            #destinationfoldertemplate = "{}\\{}\\{}_".format(DestFolder,subfoldernamestr,subfoldernamestr.replace("\\",""))
##            destinationfoldertemplate = os.path.join(DestFolder,subfoldernamestr,subfoldernamestr.replace(os.sep,"")) + "_"
##            resulttablequery = "INSERT INTO {} (launch_id,SourceFilepath,DestFilepath) SELECT {}, filepath, '{}' || substr('00000'||NPP,-5,5) FROM {}".format(Dbhelper.FILES_FOR_MOVING_TAB_NAME, launchid, destinationfoldertemplate, temptabname)
##            #print(resulttablequery) 
##            connection.execute(resulttablequery)
##           
##        connection.commit()
##        connection.close()
##        #print(temptabname) 
##        #filenameformat = "{}{}_{}"
##
##        
##    def getfilesformoving(self, launchid):
##        connection = sq.connect(Dbhelper.DB_FILENAME)
##        #Return all files that should be moved from source and files that sould be renamed in destnation folder
##
##        query = "SELECT SourceFilepath, DestFilepath FROM {} WHERE launch_id = ? AND SourceFilepath<>DestFilepath".format(Dbhelper.FILES_FOR_MOVING_TAB_NAME)
##        cursor = connection.cursor()
##        cursor.execute(query, (launchid,))
##        results = cursor.fetchall()        
##        connection.close()
##        return results


class Uploader():
    '''
    Basic class that proivides all methods for uploading and maintainance data in photo storage
    '''
    AVAILABLE_EXTENSIONS_FOR_DATE_EXTRACTING = (".JPG",".JPEG")
    
    def __init__(self,destfolder,deletesource = False):
        '''
        destfolder is the root folder of the storage
        '''
        self.rootfolder = destfolder
        if not os.path.exists(destfolder):
                os.makedirs(destfolder)
        self.dbhelper = Dbhelper(destfolder)
        self.deletesource = deletesource
        self.launch_id = None

    def __createlaunch__(self,params):
        self.launch_id = self.dbhelper.create_launch(params)


    def __md5__(self,fname):
        '''
        Returns md5 hashsum of a file
        '''
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()


##    def __uploadfile__(self, filepath):
##        '''
##        Uploads file with known data to the storage. returns id of added file
##        '''
##        #fileinfo = self.__getfileinfofromfilesystem__(filepath)
##        #TO_DO Use kwargs?
##        newfileid = self.dbhelper.insertnewfileinfo(fileinfo[0],fileinfo[1],fileinfo[2],fileinfo[3],fileinfo[4],fileinfo[5])
##        return newfileid

    def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
        filledLength = int(length * iteration // total)
        bar = fill * filledLength + '-' * (length - filledLength)
        print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
        # Print New Line on Complete
        if iteration == total: 
            print()

    

    def __getfileinfofromfilesystem__(self,filepath):
        '''
        Returns file info as a tuple: filepath, extension, size,  filehash, creationdate,creationdatesource
        '''
        shortname, extension = os.path.splitext(filepath)
        size = os.path.getsize(filepath)
        filehash = self.__md5__(filepath)
        creationdatesource = 'File_creation_date' # 'Exif',
        extension = extension.upper()
        
        if extension in Uploader.AVAILABLE_EXTENSIONS_FOR_DATE_EXTRACTING:
            '''try to get date from exiftag'''
            exifcreatedate, exifcreatedatemilisecs = exiftag(filepath,("EXIF DateTimeOriginal","EXIF SubSecTimeOriginal"))
            
            if not exifcreatedate is None:
                if not exifcreatedatemilisecs is None:
                    creationdate = datetime.datetime.strptime("{}.{}".format(exifcreatedate,exifcreatedatemilisecs), "%Y:%m:%d %H:%M:%S.%f")
                else:
                    creationdate = datetime.datetime.strptime(exifcreatedate, "%Y:%m:%d %H:%M:%S") 
                creationdatesource = 'Exif'

        '''
        creationdatesource will be 'Exif', if extracted succesful, if not extracting from file creation date
        '''
        if creationdatesource == 'File_creation_date':
                '''windows only'''
                
                #creationtime = time.localtime(os.path.getctime(filepath))
                #print(creationtime)
                #creationdate = time.strftime("%Y-%m-%d %H:%M:%S", creationtime)
                creationdate = datetime.datetime.fromtimestamp(os.path.getctime(filepath)) 
                
        return (filepath, extension, size,  filehash, creationdate, creationdatesource )


##    def __getsourcefileinfo__(self,launchid, source_file_id):
##        '''
##        gets file information from table that stores info about files we want to upload
##        '''
        
        
    
    def loaddatafromsource(self,sourcepath):
        '''
        loads data from source folder
        '''
        if self.launch_id  is None:
            self.__createlaunch__(None)

        
        for dirname, dirnames, filenames in os.walk(sourcepath):
            print("Processing folder {}".format(dirname))
            # print path to all filenames.
            for filename in filenames:
                fullfilename = os.path.join(dirname, filename)
                print(fullfilename)
                fileinfo = self.__getfileinfofromfilesystem__(fullfilename)
                #print(fileinfo)
                self.dbhelper.insertsourcefileinfo(self.launch_id,fileinfo[0],fileinfo[1],fileinfo[2],fileinfo[3],fileinfo[4],fileinfo[5] )
                #filescount +=1 
        self.dbhelper.commit()
        self.dbhelper.markduplicatesinsource(self.launch_id)
        self.dbhelper.fillqueue(self.launch_id)


    def processfiles(self):
        '''
        moves and uploades all files to the datastore root
        '''
        queue =  self.dbhelper.getqueue(self.launch_id)

        totalfilesforprocessing = len(queue)
        processedindex = 0
        '''
        Files for moving cannot be moved directly, beecause in some situations two files names should be intercahnged
        First rename this files to temporary unique name, afer uploading moving to destination 
        '''
        #list of tuple (tempfilename, destfilename)
        filesformovingtemporarylist = []


        for fileinfo in queue:
            #print("processing file:", fileinfo)
            file_id = fileinfo[1]
            source_file_id  = fileinfo[2]
            action = fileinfo[3]
            subfolder = fileinfo[4]
            filename = fileinfo[5]
            
            destfilepath = os.path.join(self.rootfolder,subfolder,filename)

            destfolder = os.path.dirname(destfilepath)
            if not os.path.exists(destfolder):
                os.makedirs(destfolder)
            
            if action == 'Move' and not file_id is None:
                uploadfileinfo = self.dbhelper.getfileinfo(file_id)
                filepath =  os.path.join(self.rootfolder,uploadfileinfo[1],uploadfileinfo[0])
                tempfilepath = os.path.join(self.rootfolder,uploadfileinfo[1],str(file_id)+ '_' +uploadfileinfo[0])
                
                #Move file
                if not os.path.exists(tempfilepath):
                    shutil.move(filepath,tempfilepath)
                    #Saving temp file info, we will move it to destination after uploading new files
                    tempfiletuple = (tempfilepath,destfilepath)
                    filesformovingtemporarylist.append(tempfiletuple) 
                    '''
                    if not os.path.exists(destfilepath):
                        shutil.move(filepath,destfilepath)
                    else:
                        #progress bar ends with \n, need to force go to new line
                        print('\r\n')
                        raise Exception("File already exists. file_id=" + str(file_id))
                    '''
                    #TO-DO change file location only after moving to destination, or change twice:
                    #First to temporary file, second to final Destination
                    self.dbhelper.changefiledestination(file_id, subfolder, filename)
                else:
                    print('\r\n')
                    raise Exception("File already exists. file_id=" + str(file_id) + ' Dest file=' + str(tempfilepath))


            elif action == 'Upload' and not source_file_id is None:
                sourcefileinfo = self.dbhelper.getsourcefileinfo(self.launch_id, source_file_id)
                sourcefilepath = sourcefileinfo[0]
                extension = sourcefileinfo[1]
                size = sourcefileinfo[2]
                filehash = sourcefileinfo[3]
                creationdate = sourcefileinfo[4]
                creationdatesource = sourcefileinfo[5]
                '''copy file'''
                if not os.path.exists(destfilepath):
                    if self.deletesource:
                        shutil.move(sourcefilepath,destfilepath)
                    else:
                        #print("source=",sourcefilepath," dest=",destfilepath) 
                        shutil.copy2(sourcefilepath,destfilepath)
                else:
                    print('\r\n')
                    raise Exception("File already exists. source_file_id=" + str(source_file_id))
                
                '''insert to database'''
                self.dbhelper.insertnewfileinfo(self.launch_id, filename, extension, size, filehash, creationdate, creationdatesource, subfolder)
                
            else:
                #To-Do error
                raise("Error copying file...")
            processedindex += 1
            Uploader.printProgressBar(processedindex,totalfilesforprocessing, length = 60,prefix = "{}/{}".format(processedindex,totalfilesforprocessing))
        #Moving temporary files to destination
        print('Processing temporary files...')
        for tempfileformove in filesformovingtemporarylist:
            tempfilepath = tempfileformove[0]
            destfilepath = tempfileformove[1]
            print('Moving file {} to {}'.format(tempfilepath,destfilepath),end=' ')
            if not os.path.exists(destfilepath):
                shutil.move(tempfilepath,destfilepath)
                print('Ok')
            else:
                raise Exception("File already exists. destfilepath=" + destfilepath)    
            
            
        print('finished')
            
'''
Checking params
'''
def checkparams(params):
    SourceFolder = params['SourceFolder']
    Destfolder = params['DestFolder']
    
    if (SourceFolder is None) or (SourceFolder ==''):
        print("Source folder not set!")
        return False
    if not os.path.exists(SourceFolder):
        print("Source folder not exists!")
        return False
    if  (Destfolder is None) or (Destfolder ==''):
        print("Destination folder not set!")
        return False
    return True   



def exiftag_old(fname,tagname):  
    f = open(fname, 'rb')
    tags = exifread.process_file(f)
    if tagname in tags:
        res = tags[tagname].values
    else:
        res = None
    f.close()
    return res

def exiftag(fname,tagnames):  
    with open(fname, 'rb') as f:
        tags = exifread.process_file(f)
        res = [tags.get(tag) for tag in tagnames]
        return tuple(tagval.values if not tagval is None else None for tagval in res)
    



# Print iterations progress



if __name__ == '__main__':
    #print("aa")
    uploader = Uploader(r'\\192.168.1.3\Nest data\All photo')
    #print(help(uploader))

    #print(uploader.__uploadfile__(r"D:\Python projects\SQLiteDatabaseBrowserPortable\help.html"))
    uploader.loaddatafromsource(r"D:\Фото для загрузки\2019-11-16")
    uploader.processfiles()

    raise("quit...")
                                         

    #creating dict for storing params
    paramsdict = dict.fromkeys(['SourceFolder','DestFolder','DisableGUI','SkipCheckDuplicates'])
    paramsdict['DisableGUI'] = False
    #paramsdict['CheckDuplicates'] = True


    description = 'Uploads photos to the file share'


    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('source',  action='store', help='Source folder', nargs="?")
    parser.add_argument('destination',  action='store', help='Destination folder', nargs="?")
    parser.add_argument('--skipcheckduplicates',  action='store_true', default = False, help='If set, duplicate files will not be copied. Check based on MD5 HASH')

    #reading params from commandline
    args = parser.parse_args()
    paramsdict['SourceFolder'] = args.source
    paramsdict['DestFolder'] = args.destination
    paramsdict['SkipCheckDuplicates']  = args.skipcheckduplicates
    #print(not args.skipcheckduplicates)


    #print(paramsdict)
    #print(checkparams(paramsdict))
    if checkparams(paramsdict):
        print("Settings correct, starting upload...")
        Sourcefolder = paramsdict['SourceFolder']
        DestFolder = paramsdict['DestFolder']
        CheckDuplicates = not paramsdict['SkipCheckDuplicates']

        
        
        
        #create_history_database_if_not_exixst()
        db = Dbhelper()

        launchid= db.create_launch(paramsdict)

        #Creating destination folder
        if not os.path.exists(DestFolder):
            os.makedirs(DestFolder)

        #Creating duplicates folder
        DuplicatesFolder =  os.path.join(Sourcefolder, 'Duplicates') #Sourcefolder + "\\Duplicates"
        if not os.path.exists(DuplicatesFolder):
            os.makedirs(DuplicatesFolder)
            

        #Reading files and storing information to database
        filescount = 0
        db.start_batch()
        for dirname, dirnames, filenames in os.walk(Sourcefolder):
            print("Processing folder {}".format(dirname))
            # print path to all filenames.
            for filename in filenames:
                fullfilename = os.path.join(dirname, filename)
                db.insert_fileinfo(launchid,fullfilename, withchecksum = CheckDuplicates, source = True)
                #filehash = md5(fullfilename)
                #print("fullfilename={} fileExtension = {} hash={}".format(fullfilename,fileExtension,filehash))
                filescount +=1
        db.end_batch()
        print("Total files found:{}".format(filescount))
        print("Checking existed files in destination folder...")

        db.start_batch()
        for dirname, dirnames, filenames in os.walk(DestFolder):
            print("Processing folder {}".format(dirname)) 
            # print path to all filenames.
            for filename in filenames:
                fullfilename = os.path.join(dirname, filename)
                db.insert_fileinfo(launchid,fullfilename, withchecksum = CheckDuplicates, source = False)
                
        db.end_batch()


        db.generatesubfolderpath(launchid)
        db.markduplicatefiles(launchid,CheckDuplicates)
        db.fillResultTable(launchid,DestFolder,DuplicatesFolder)
        
        '''
        filesformoving = db.getfilesformoving(launchid)
        #print("len=" + str(len(filesformoving)))
        filesformovingcount = len(filesformoving)
        currentfileindex = 0
        printProgressBar(currentfileindex, filesformovingcount, prefix = 'Copying:', suffix = 'Complete', length = 50)
        for fileinfo in filesformoving:
           sourcefilepath= fileinfo[0]
           destfilepath= fileinfo[1]
           currentfileindex += 1
           printProgressBar(currentfileindex, filesformovingcount, prefix = 'Copying:', suffix = 'Complete', length = 50)
           #os.rename(sourcefilepath, destfilepath)
        #print(filesformoving) 
        '''

        

    '''
    if not paramsdict['DisableGUI']:

        top = Tk()
        #resultvar = StringVar()
        #top.resizable(0,0)
        #Source Folder
        sourcelabel = Label(top, text= "Source folder")
        sourcelabel.grid(row=0, column=0)
        sourceentry = Entry(top,show="*")
        sourceentry.grid(row=0, column=1)
        #Destination Folder
        destinationlabel = Label(top, text= "Destination folder")
        destinationlabel.grid(row=1, column=0)
        destinationentry = Entry(top,show="*")
        destinationentry.grid(row=1, column=1)
        #Buttons
        #buttonframe = Frame(top, bg="green")
        top.mainloop()
    '''



    #print(paramsdict)





    #print("Ok")
    '''
    Sourcefolder = r"D:\DCIM\test"
    DestFolder = r"D:\DCIM\test_uploaded\1\2\3\4\4\5\6\yyy\h"



    '''
        
